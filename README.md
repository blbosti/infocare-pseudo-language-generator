# infocare-pseudo-language-generator

Generates a pseudo language localisation file from an i18n JSON file

## Installation

` [sudo] npm -g install infocare-pseudo-language-generator`

## CLI usage

`infocare-pseudo-language-generator --source=[sourcePath] --destination=[destinationPath]`
Creates a pseudo JSON file in which each key-value pair is copied from the sourcePath with the 
addition of each value having the string "XXX" prepended and appended to it

_Example_
`infocare-pseudo-language-generator --source=./src/lang/en.json --destination=./src/lang/tr.json`
