#!/usr/bin/env node

// NPM dependencies
const minimist = require('minimist');
const infocarePseudoLanguageGenerator = require('./');

const PSEUDO_CONTENT = 'XXX';

var argv = minimist(process.argv.slice(2));

if (argv && argv.source && argv.destination) {
  infocarePseudoLanguageGenerator.generate(argv.source, argv.destination, PSEUDO_CONTENT);
} else {
  console.log("infocare-pseudo-language-generator: `source` and `destination` arguments are required")
}
