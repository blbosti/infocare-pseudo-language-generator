const fs = require('fs');

function generatePsuedoJsonFile (sourcePath, destinationPath, pseudoContent) {
  let rawdata = fs.readFileSync(sourcePath);
  let existingJson = JSON.parse(rawdata);
  let pseudoJson = {}

  for( let prop in existingJson ){
    pseudoJson[prop] =  pseudoContent + existingJson[prop] + pseudoContent;
  }

  //extra parameters add new lines and indentation
  let data = JSON.stringify(pseudoJson, null, 2) + '\n';
  fs.writeFileSync(destinationPath, data);
}

module.exports = generatePsuedoJsonFile;